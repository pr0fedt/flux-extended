'use strict';
import _ from 'lodash';
import _legacyConstructor from './_legacyActionCreatorConstructor'
import isPromise from '../lib/isPromise';
import DispatcherSingleton from './_dispatcherSingleton';

const dispatcher = DispatcherSingleton.instance;
const actions = Symbol();;

const SUCCESS_POSTFIX = 'Success';
const FAIL_POSTFIX = 'Fail';

const DEFAULT_SERVICE_SOURCE = 'Service';
const FAIL_SOURCE = 'Error';

const DEFERRED_ACTION = "Defer";

class Action{
    constructor(actionType){
        this.actionType = actionType;
        this._service = null;
    }

    withService(service){
        this._service = service;
        this.successActionType = this.actionType + SUCCESS_POSTFIX;
        this.failActionType = this.actionType + FAIL_POSTFIX;
        this.serviceSource = DEFAULT_SERVICE_SOURCE;
        return this;
    }

    withServiceSource(source){
        this.serviceSource = source;
        return this;
    }

    withTypes(...responseTypes){
        this.successActionType = responseTypes[0];
        this.failActionType = responseTypes[1];
        return this;
    }

    exec(action){
        let actionType = this.actionType;
        dispatcher.dispatch({
            actionType,
            action
        });
        let service = this._service;
        if(_.isFunction(service)){
            let servicePromise = service(action);
            if(!isPromise(servicePromise)){
                throw new Error('Wrong service');
            }else{
                servicePromise.then(
                    data => dispatcher.dispatch.call(
                        dispatcher, {
                            actionType: this.successActionType,
                            action, data
                        },
                        this.serviceSource
                    ), reason => dispatcher.dispatch.call(
                        dispatcher, {
                            actionType: this.failActionType,
                            action, reason
                        },
                        FAIL_SOURCE
                    )
                );
            }
        }
    }
}

export class ActionCreator {
    constructor(actionHandlers){
        if(!_.isUndefined(actionHandlers)){
            console.warn(
`Using deprecated action creator constructor(with options).
Better to use __ActionCreator.prototype.createAction__ instead.`
            );
            _legacyConstructor.call(this, actionHandlers);
        }
    }

    createAction(actionName, actionType){
        let action = new Action(actionType);
        Object.defineProperty(this, actionName, {
            get: () => action.exec.bind(action)
        });
        Object.defineProperty(this, actionName + DEFERRED_ACTION, {
            get: () => (actionData => 
                setTimeout(action.exec.bind(action, actionData), 0)
            )
        });

	    return action;
    }
}
