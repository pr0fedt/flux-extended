'use strict';

import _ from 'lodash';
import DispatcherSingleton from './_dispatcherSingleton';

const dispatcher = DispatcherSingleton.instance;

function _createAction(options){
    let action = {};
    function successCallback(data){
        var actionType = options.actionType + 'Success';
        if(_.isString(options.successActionType)){
            actionType = options.successActionType;
        }
        dispatcher.dispatch({
            actionType,
            action, data
        }, 'Server');
    }
    function failCallback(reason){
        var actionType = options.actionType + 'Error';
        if(_.isString(options.failActionType)){
            actionType = options.failActionType;
        }
        dispatcher.dispatch({
            actionType, data, reason
        }, 'Error');
    }
    return function(dispatchData){
        let promise = null;
        action = dispatchData ? dispatchData : {};
        if(_.isString(options.actionType)){
            dispatcher.dispatch({
                actionType: options.actionType,
                action
            });
            if(_.isFunction(options.service)){
                promise = options.service(action);
                if(_.isFunction(promise.then)){
                    promise.then(successCallback, failCallback);
                }else{
                    throw new Error('Service must return a valid promise');
                }
            }
        }else{
            throw new Error('missing action type(options.actionType is required)');
        }
    };
}

export default function (actions){
    _.forEach(actions, (actionOptions, key) => Object.defineProperty(
            this,
            key, {
                get: function(){
                    return _createAction(actionOptions);
                }
            }
        )
    );
}