'use strict';

import _ from 'lodash';
import {Dispatcher} from 'flux';

const dispatcher = Symbol();
const log = Symbol();
const prevAction = Symbol();
const queue = Symbol();
const singleton = Symbol();
const singletonInit = Symbol();
const cto = Symbol();

const DEFAULT_SOURCE = 'View';
const CHECK_TIMEOUT = 200;
export default class DispatcherSingleton{
    constructor(initSymbol){
        if(initSymbol !== singletonInit){
            throw new Error('Cannot construct');
        }else{
            this[dispatcher] = new Dispatcher();
            this[log] = null;
            this[prevAction] = null;
            this[queue] = [];
            this[cto] = CHECK_TIMEOUT;
        }
    }

    static get instance(){
        if(!this[singleton]){
            this[singleton] = new DispatcherSingleton(singletonInit);
        }else{
            return this[singleton];
        }
    }
    set checkTimeout(ms){
        this[cto] = ms;
    }
    set logger(logFunction){
        this[log] = logFunction;
    }
    checkQueue(){
        let actionDescriptor = this[queue].pop();
        if(actionDescriptor){
            let {action, source} = actionDescriptor;
            this.dispatch(action, source);
        }
        if(this[queue].length){
            setTimeout(this.checkQueue.bind(this), this[cto]);
        }

    }
    dispatch(action, source){
        let _dispatcher = this[dispatcher];
        let dispatchSource = source ? source : DEFAULT_SOURCE;
        if(this[prevAction] && _dispatcher.isDispatching()){
            let dispatchingAction = this[prevAction];
            this[queue].unshift({action, source});
            console.warn(
`Possible design flaw: [Action]
${JSON.stringify(dispatchingAction, null, 2)} is currently dispatching
***********************
${source} Action ${JSON.stringify(action, null, 2)} has been queued.`
            );
            setTimeout(this.checkQueue.bind(this), this[cto]);
        }else {
            this[prevAction] = action;
            this[dispatcher].dispatch(action);
            if(_.isFunction(this[log])){
                this[log]({
                    action, dispatchSource
                });    
            }
        }
    }

    register(dispatchHandler){
        return this[dispatcher].register(dispatchHandler);
    }

    waitFor(tokens){
        if(_.isString(tokens)){
            this[dispatcher].waitFor([tokens]);
        } else if(_.isArray(tokens)){
            this[dispatcher].waitFor(tokens);
        }
    }
}