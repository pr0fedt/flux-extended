'use strict';

import _ from 'lodash';
import {Store} from './index';

const STORE_POSTFIX = 'Store';
const STORE_HANDLER_METHOD_POSTFIX = 'StoreChanged';

export class ReactStore extends Store{
    constructor(...args){
        super(...args);
    }

    getMixin(storeName){
        let store = this;
        let storeProperty = `${storeName}${STORE_POSTFIX}`;
        let storeHandlerMethodName = `${storeName}${STORE_HANDLER_METHOD_POSTFIX}`;
        let mixin = {
            componentDidMount(){
                let storeHandler = this[storeHandlerMethodName];
                if(_.isFunction(storeHandler)){
                    store.subscribe(storeHandler);
                }else{
                    console.warn(
`Component <${this.constructor.displayName}> has no [${storeHandlerMethodName}] method`
                    );
                }
            },
            componentWillUnmount(){
                let store = this[storeProperty];
                let storeHandler = this[storeHandlerMethodName];
                if(_.isFunction(storeHandler)){
                    store.unsubscribe(storeHandler);
                }
            }
        };
        
        mixin[storeProperty] = this;
        return mixin;
    }
};