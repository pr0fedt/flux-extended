'use strict';
import _ from 'lodash';
import {EventEmitter} from 'events';

import invariant from 'flux/lib/invariant';
import DispatcherSingleton from '../_dispatcherSingleton';

const handlers = Symbol();
const token = Symbol();

const dispatcher = DispatcherSingleton.instance;
const CHANGE_EVENT = 'STORE_CHANGED';
export class Store extends EventEmitter{
    constructor(initialValue, options){
        super();

        this[handlers] = {};
        this._data = initialValue;

        let storeOptions = options ? options : {};
        _.forEach(options, (option, key) => {
            switch(key){
                case 'extend':
                    _.forEach(option, (fn, name) => this[name] = fn.bind(this));
                    break;
                default:
            }
        });
        this[token] = dispatcher.register(payload => 
            _.map(this[handlers], (handler, actionType) => 
                actionType == payload.actionType ? handler(payload) : true
            )
        );
    }
    setHandler(actionType, handler){
        this[handlers][actionType] = handler.bind(this);
    }
    subscribe(handler){
        this.addListener(CHANGE_EVENT, handler);
    }
    unsubscribe(handler){
        this.removeListener(CHANGE_EVENT, handler);
    }
    _emit(){
        setTimeout(this.emit.bind(null, CHANGE_EVENT), 0);
    }
    wait(){
        dispatcher.waitFor(this[token]);
    }
}
export {ReactStore} from './ReactStore';
