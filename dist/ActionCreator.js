'use strict';

var _createClass = require('babel-runtime/helpers/create-class')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _Symbol = require('babel-runtime/core-js/symbol')['default'];

var _Object$defineProperty = require('babel-runtime/core-js/object/define-property')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _legacyActionCreatorConstructor = require('./_legacyActionCreatorConstructor');

var _legacyActionCreatorConstructor2 = _interopRequireDefault(_legacyActionCreatorConstructor);

var _libIsPromise = require('../lib/isPromise');

var _libIsPromise2 = _interopRequireDefault(_libIsPromise);

var _dispatcherSingleton = require('./_dispatcherSingleton');

var _dispatcherSingleton2 = _interopRequireDefault(_dispatcherSingleton);

var dispatcher = _dispatcherSingleton2['default'].instance;
var actions = _Symbol();;

var SUCCESS_POSTFIX = 'Success';
var FAIL_POSTFIX = 'Fail';

var DEFAULT_SERVICE_SOURCE = 'Service';
var FAIL_SOURCE = 'Error';

var DEFERRED_ACTION = 'Defer';

var Action = (function () {
    function Action(actionType) {
        _classCallCheck(this, Action);

        this.actionType = actionType;
        this._service = null;
    }

    _createClass(Action, [{
        key: 'withService',
        value: function withService(service) {
            this._service = service;
            this.successActionType = this.actionType + SUCCESS_POSTFIX;
            this.failActionType = this.actionType + FAIL_POSTFIX;
            this.serviceSource = DEFAULT_SERVICE_SOURCE;
            return this;
        }
    }, {
        key: 'withServiceSource',
        value: function withServiceSource(source) {
            this.serviceSource = source;
            return this;
        }
    }, {
        key: 'withTypes',
        value: function withTypes() {
            this.successActionType = arguments[0];
            this.failActionType = arguments[1];
            return this;
        }
    }, {
        key: 'exec',
        value: function exec(action) {
            var _this = this;

            var actionType = this.actionType;
            dispatcher.dispatch({
                actionType: actionType,
                action: action
            });
            var service = this._service;
            if (_lodash2['default'].isFunction(service)) {
                var servicePromise = service(action);
                if (!(0, _libIsPromise2['default'])(servicePromise)) {
                    throw new Error('Wrong service');
                } else {
                    servicePromise.then(function (data) {
                        return dispatcher.dispatch.call(dispatcher, {
                            actionType: _this.successActionType,
                            action: action, data: data
                        }, _this.serviceSource);
                    }, function (reason) {
                        return dispatcher.dispatch.call(dispatcher, {
                            actionType: _this.failActionType,
                            action: action, reason: reason
                        }, FAIL_SOURCE);
                    });
                }
            }
        }
    }]);

    return Action;
})();

var ActionCreator = (function () {
    function ActionCreator(actionHandlers) {
        _classCallCheck(this, ActionCreator);

        if (!_lodash2['default'].isUndefined(actionHandlers)) {
            console.warn('Using deprecated action creator constructor(with options).\nBetter to use __ActionCreator.prototype.createAction__ instead.');
            _legacyActionCreatorConstructor2['default'].call(this, actionHandlers);
        }
    }

    _createClass(ActionCreator, [{
        key: 'createAction',
        value: function createAction(actionName, actionType) {
            var action = new Action(actionType);
            _Object$defineProperty(this, actionName, {
                get: function get() {
                    return action.exec.bind(action);
                }
            });
            _Object$defineProperty(this, actionName + DEFERRED_ACTION, {
                get: function get() {
                    return function (actionData) {
                        return setTimeout(action.exec.bind(action, actionData), 0);
                    };
                }
            });

            return action;
        }
    }]);

    return ActionCreator;
})();

exports.ActionCreator = ActionCreator;