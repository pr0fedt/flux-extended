'use strict';

var _createClass = require('babel-runtime/helpers/create-class')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _Symbol = require('babel-runtime/core-js/symbol')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _flux = require('flux');

var dispatcher = _Symbol();
var log = _Symbol();
var prevAction = _Symbol();
var queue = _Symbol();
var singleton = _Symbol();
var singletonInit = _Symbol();
var cto = _Symbol();

var DEFAULT_SOURCE = 'View';
var CHECK_TIMEOUT = 200;

var DispatcherSingleton = (function () {
    function DispatcherSingleton(initSymbol) {
        _classCallCheck(this, DispatcherSingleton);

        if (initSymbol !== singletonInit) {
            throw new Error('Cannot construct');
        } else {
            this[dispatcher] = new _flux.Dispatcher();
            this[log] = null;
            this[prevAction] = null;
            this[queue] = [];
            this[cto] = CHECK_TIMEOUT;
        }
    }

    _createClass(DispatcherSingleton, [{
        key: 'checkQueue',
        value: function checkQueue() {
            var actionDescriptor = this[queue].pop();
            if (actionDescriptor) {
                var action = actionDescriptor.action;
                var source = actionDescriptor.source;

                this.dispatch(action, source);
            }
            if (this[queue].length) {
                setTimeout(this.checkQueue.bind(this), this[cto]);
            }
        }
    }, {
        key: 'dispatch',
        value: function dispatch(action, source) {
            var _dispatcher = this[dispatcher];
            var dispatchSource = source ? source : DEFAULT_SOURCE;
            if (this[prevAction] && _dispatcher.isDispatching()) {
                var dispatchingAction = this[prevAction];
                this[queue].unshift({ action: action, source: source });
                console.warn('Possible design flaw: [Action]\n' + JSON.stringify(dispatchingAction, null, 2) + ' is currently dispatching\n***********************\n' + source + ' Action ' + JSON.stringify(action, null, 2) + ' has been queued.');
                setTimeout(this.checkQueue.bind(this), this[cto]);
            } else {
                this[prevAction] = action;
                this[dispatcher].dispatch(action);
                if (_lodash2['default'].isFunction(this[log])) {
                    this[log]({
                        action: action, dispatchSource: dispatchSource
                    });
                }
            }
        }
    }, {
        key: 'register',
        value: function register(dispatchHandler) {
            return this[dispatcher].register(dispatchHandler);
        }
    }, {
        key: 'waitFor',
        value: function waitFor(tokens) {
            if (_lodash2['default'].isString(tokens)) {
                this[dispatcher].waitFor([tokens]);
            } else if (_lodash2['default'].isArray(tokens)) {
                this[dispatcher].waitFor(tokens);
            }
        }
    }, {
        key: 'checkTimeout',
        set: function set(ms) {
            this[cto] = ms;
        }
    }, {
        key: 'logger',
        set: function set(logFunction) {
            this[log] = logFunction;
        }
    }], [{
        key: 'instance',
        get: function get() {
            if (!this[singleton]) {
                this[singleton] = new DispatcherSingleton(singletonInit);
            } else {
                return this[singleton];
            }
        }
    }]);

    return DispatcherSingleton;
})();

exports['default'] = DispatcherSingleton;
module.exports = exports['default'];