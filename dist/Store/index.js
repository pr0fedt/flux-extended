'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _get = require('babel-runtime/helpers/get')['default'];

var _createClass = require('babel-runtime/helpers/create-class')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _Symbol = require('babel-runtime/core-js/symbol')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _events = require('events');

var _fluxLibInvariant = require('flux/lib/invariant');

var _fluxLibInvariant2 = _interopRequireDefault(_fluxLibInvariant);

var _dispatcherSingleton = require('../_dispatcherSingleton');

var _dispatcherSingleton2 = _interopRequireDefault(_dispatcherSingleton);

var handlers = _Symbol();
var token = _Symbol();

var dispatcher = _dispatcherSingleton2['default'].instance;
var CHANGE_EVENT = 'STORE_CHANGED';

var Store = (function (_EventEmitter) {
    function Store(initialValue, options) {
        var _this = this;

        _classCallCheck(this, Store);

        _get(Object.getPrototypeOf(Store.prototype), 'constructor', this).call(this);

        this[handlers] = {};
        this._data = initialValue;

        var storeOptions = options ? options : {};
        _lodash2['default'].forEach(options, function (option, key) {
            switch (key) {
                case 'extend':
                    _lodash2['default'].forEach(option, function (fn, name) {
                        return _this[name] = fn.bind(_this);
                    });
                    break;
                default:
            }
        });
        this[token] = dispatcher.register(function (payload) {
            return _lodash2['default'].map(_this[handlers], function (handler, actionType) {
                return actionType == payload.actionType ? handler(payload) : true;
            });
        });
    }

    _inherits(Store, _EventEmitter);

    _createClass(Store, [{
        key: 'setHandler',
        value: function setHandler(actionType, handler) {
            this[handlers][actionType] = handler.bind(this);
        }
    }, {
        key: 'subscribe',
        value: function subscribe(handler) {
            this.addListener(CHANGE_EVENT, handler);
        }
    }, {
        key: 'unsubscribe',
        value: function unsubscribe(handler) {
            this.removeListener(CHANGE_EVENT, handler);
        }
    }, {
        key: '_emit',
        value: function _emit() {
            this.emit(CHANGE_EVENT);
        }
    }, {
        key: 'wait',
        value: function wait() {
            dispatcher.waitFor(this[token]);
        }
    }]);

    return Store;
})(_events.EventEmitter);

exports.Store = Store;

var _ReactStore = require('./ReactStore');

Object.defineProperty(exports, 'ReactStore', {
    enumerable: true,
    get: function get() {
        return _ReactStore.ReactStore;
    }
});