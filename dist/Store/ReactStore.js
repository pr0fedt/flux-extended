'use strict';

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _get = require('babel-runtime/helpers/get')['default'];

var _createClass = require('babel-runtime/helpers/create-class')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _index = require('./index');

var STORE_POSTFIX = 'Store';
var STORE_HANDLER_METHOD_POSTFIX = 'StoreChanged';

var ReactStore = (function (_Store) {
    function ReactStore() {
        _classCallCheck(this, ReactStore);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        _get(Object.getPrototypeOf(ReactStore.prototype), 'constructor', this).apply(this, args);
    }

    _inherits(ReactStore, _Store);

    _createClass(ReactStore, [{
        key: 'getMixin',
        value: function getMixin(storeName) {
            var store = this;
            var storeProperty = '' + storeName + STORE_POSTFIX;
            var storeHandlerMethodName = '' + storeName + STORE_HANDLER_METHOD_POSTFIX;
            var mixin = {
                componentDidMount: function componentDidMount() {
                    var storeHandler = this[storeHandlerMethodName];
                    if (_lodash2['default'].isFunction(storeHandler)) {
                        store.subscribe(storeHandler);
                    } else {
                        console.warn('Component <' + this.constructor.displayName + '> has no [' + storeHandlerMethodName + '] method');
                    }
                },
                componentWillUnmount: function componentWillUnmount() {
                    var store = this[storeProperty];
                    var storeHandler = this[storeHandlerMethodName];
                    if (_lodash2['default'].isFunction(storeHandler)) {
                        store.unsubscribe(storeHandler);
                    }
                }
            };

            mixin[storeProperty] = this;
            return mixin;
        }
    }]);

    return ReactStore;
})(_index.Store);

exports.ReactStore = ReactStore;
;