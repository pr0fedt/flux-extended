'use strict';

var _Object$defineProperty = require('babel-runtime/core-js/object/define-property')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
    value: true
});

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _dispatcherSingleton = require('./_dispatcherSingleton');

var _dispatcherSingleton2 = _interopRequireDefault(_dispatcherSingleton);

var dispatcher = _dispatcherSingleton2['default'].instance;

function _createAction(options) {
    var action = {};
    function successCallback(data) {
        var actionType = options.actionType + 'Success';
        if (_lodash2['default'].isString(options.successActionType)) {
            actionType = options.successActionType;
        }
        dispatcher.dispatch({
            actionType: actionType,
            action: action, data: data
        }, 'Server');
    }
    function failCallback(reason) {
        var actionType = options.actionType + 'Error';
        if (_lodash2['default'].isString(options.failActionType)) {
            actionType = options.failActionType;
        }
        dispatcher.dispatch({
            actionType: actionType, data: data, reason: reason
        }, 'Error');
    }
    return function (dispatchData) {
        var promise = null;
        action = dispatchData ? dispatchData : {};
        if (_lodash2['default'].isString(options.actionType)) {
            dispatcher.dispatch({
                actionType: options.actionType,
                action: action
            });
            if (_lodash2['default'].isFunction(options.service)) {
                promise = options.service(action);
                if (_lodash2['default'].isFunction(promise.then)) {
                    promise.then(successCallback, failCallback);
                } else {
                    throw new Error('Service must return a valid promise');
                }
            }
        } else {
            throw new Error('missing action type(options.actionType is required)');
        }
    };
}

exports['default'] = function (actions) {
    var _this = this;

    _lodash2['default'].forEach(actions, function (actionOptions, key) {
        return _Object$defineProperty(_this, key, {
            get: function get() {
                return _createAction(actionOptions);
            }
        });
    });
};

module.exports = exports['default'];