'use strict';
import gulp from 'gulp';
import babel from 'gulp-babel';

gulp.task('default', ['compile']);
gulp.task(
    'compile',
    () => gulp.src('src/**/*.js')
    .pipe(babel({optional: ['runtime']}))
    .pipe(gulp.dest('dist'))
);
gulp.task(
    'watch',
    () => gulp.watch(
        'src/**/*.js',
        ['compile']
    )
);
