'use strict';
/**
 * Polyfill for checking promise,
 * see if methods 'then', 'catch' exist in Promise.prototype
 * and if methods 'all', 'race', 'reject', 'resolve' exist in Promise.prototype.constructor */
var PROMISE_PROTOTYPE_METHODS = ['then', 'catch'];
var PROMISE_CONSTRUCT_METHODS = ['all', 'race', 'reject', 'resolve'];

module.exports = function isPromise(value){
    return PROMISE_PROTOTYPE_METHODS.every(function(pMethod){
        return typeof value[pMethod] === 'function';
    }) && PROMISE_CONSTRUCT_METHODS.every(function(cMethod){
        return typeof value.constructor[cMethod] === 'function';
    });
};